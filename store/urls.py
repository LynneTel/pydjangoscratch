from django.conf.urls import url
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    url(r'^index/$', TemplateView.as_view(template_name='template.html')),
    # url(r'^$', TemplateView.as_view(template_name='store.html')),
    # url(r'^$', 'views.store', name='index'),
    url(r'^$', views.store, name='index'),
]
